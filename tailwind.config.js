/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js}"],
  theme: {
    extend: {
      colors: {
        'primary': '#525252',
        'secondary': '#a3a3a3',
        'dark': '#262626',
        'light': '#f5f5f5',
        'gray': '#d4d4d4',
        'blue': '#60a5fa',
      },
    },
    plugins: [],
  }
}
