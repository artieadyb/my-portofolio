import { Outlet } from 'react-router-dom'
import './App.css';

function App() {
  return (
    <div className="App">
        <div className='navbar flex h-12 items-center bg-primary justify-between px-10 sticky top-0'>
          <div className='text-xl text-blue font-bold'>ARTIEADY BUDIMAN</div>
          <div className='text-light'>
            <ul className='flex flex-row'>
              <li className='me-3'>
                <a href='#'>Know Me</a>
              </li>
              <li className='me-3'>
                <a href='#'>My Portofolio</a>
              </li>
              <li>
                <a href='#'>Contact Me</a>
              </li>
            </ul>
          </div>
        </div>
        <div>
          Content
          <Outlet/>
        </div>
        <div>
          Footer
        </div>
    </div>
  );
}

export default App;
